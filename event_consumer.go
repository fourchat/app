package app

// System Bus

import (
	"github.com/streadway/amqp"
	"go.uber.org/zap"
	"os"
)

type EventConsumer struct {
	Bus      *EventBus
	Messages chan []byte
}

func (c *EventConsumer) Consume() {
	log := c.Bus.Log
	log.Debug("EventBus consumer running...")
	defer func() {
		log.Debug("EventBus consumer finished...")
	}()

	conn, err := amqp.Dial(os.Getenv("FOURCHAT_AMQP_CONNECT_SPEC"))
	if err != nil {
		log.Error("cannot connect to AMQP: ", zap.Error(err))
		return
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Error("cannot open channel to AMQP: ", zap.Error(err))
		return
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		EXCHANGE_NAME, // name
		"fanout",      // type
		true,          // durable
		false,         // auto-deleted
		false,         // internal
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		log.Error("cannot open channel to AMQP: ", zap.Error(err))
		return
	}

	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   //args
	)
	if err != nil {
		log.Error("cannot create queue: ", zap.Error(err))
		return
	}

	err = ch.QueueBind(
		q.Name,        // queue name
		"",            // routing key
		EXCHANGE_NAME, // exchange
		false,
		nil,
	)
	if err != nil {
		log.Error("cannot bind to queue: ", zap.Error(err))
		return
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		log.Error("cannot consume: ", zap.Error(err))
		return
	}

	for d := range msgs {
		c.Messages <- d.Body
	}
}
