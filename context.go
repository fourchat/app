package app

import (
	"bufio"
	"bytes"
	"database/sql"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"go.uber.org/zap"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strconv"
	"sync/atomic"
	"time"
)

type AppContext struct {
	Log      *zap.Logger `json:"-"`
	Db       *sql.DB     `json:"-"`
	EventBus *EventBus   `json:"-"`

	CurrentUserId int64 `json:"-"`
}

type AppHandler struct {
	*AppContext
	H func(*AppContext, http.ResponseWriter, *http.Request)
}

type AuthlessHandler struct {
	*AppContext
	H func(*AppContext, http.ResponseWriter, *http.Request)
}

type httpResponseWriter struct {
	http.ResponseWriter
	hijacker http.Hijacker
	status   int
	length   int
}

type Logger interface {
	logRequest(func(*AppContext, http.ResponseWriter, *http.Request)) func(*httpResponseWriter, *http.Request)
}

func (w *httpResponseWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = 200
	}
	n, err := w.ResponseWriter.Write(b)
	w.length += n
	return n, err
}

func (w *httpResponseWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *httpResponseWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	if w.hijacker == nil {
		return nil, nil, errors.New("http.Hijacker not implemented by underlying http.ResponseWriter")
	}
	return w.hijacker.Hijack()
}

func (ctx *AppContext) logRequest(next func(*AppContext, http.ResponseWriter, *http.Request)) func(*httpResponseWriter, *http.Request) {
	return func(w *httpResponseWriter, r *http.Request) {
		start := time.Now()
		log := ctx.Log
		body := ``

		if os.Getenv(`APP_ENV`) != `production` {
			bytesBody, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Error(`Failed create buffer`, zap.Error(err))
				return
			}
			body = string(bytesBody[:])
			r.Body = ioutil.NopCloser(bytes.NewBuffer(bytesBody))
		}

		next(ctx, w, r)

		duration := time.Now().Sub(start)

		log.Info(r.Method+" "+r.URL.Path,
			zap.String(`method`, r.Method),
			zap.String(`clientip`, IpAddr(r)),
			zap.String(`user_agent`, r.UserAgent()),
			zap.String(`request_body`, body),
			zap.Int(`status`, w.status),
			zap.Int(`content_len`, w.length),
			zap.Int64(`duration`, int64(duration)),
		)
	}
}

func (h AppHandler) authRequest(next func(*AppContext, http.ResponseWriter, *http.Request)) func(*AppContext, http.ResponseWriter, *http.Request) {
	return func(ctx *AppContext, w http.ResponseWriter, r *http.Request) {
		log := ctx.Log
		signKey := os.Getenv(`FOURCHAT_SECRET_KEY`)
		authToken := r.Header.Get(`X-Authorization`)

		if authToken != "" {
			token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, errors.New("Error during parse jwt")
				}
				return []byte(signKey), nil
			})

			if err != nil {
				log.Debug("Error: ", zap.Error(err))
				w.WriteHeader(http.StatusUnauthorized)
				return
			}

			if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
				if userIdStr, ok := claims["sub"].(string); ok {
					userId, err := strconv.ParseInt(userIdStr, 10, 64)
					if err != nil {
						log.Debug("Error: invalid parse user id", zap.Any("val", userId))
						w.WriteHeader(http.StatusUnauthorized)
						return
					}
					ctx.CurrentUserId = userId
				} else {
					log.Debug("Error: invalid parse string user id", zap.Any("val", userIdStr), zap.Any("claims", claims))
					w.WriteHeader(http.StatusUnauthorized)
					return
				}

				next(ctx, w, r)
			} else {
				log.Debug("Error: invalid jwt")
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		} else {
			log.Debug("Error: empty header")
			w.WriteHeader(http.StatusUnauthorized)
		}
	}
}

func (h AppHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	hijacker := w.(http.Hijacker)
	sw := httpResponseWriter{ResponseWriter: w, hijacker: hijacker}
	h.logRequest(h.authRequest(h.H))(&sw, r)
}

func (h AuthlessHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	hijacker := w.(http.Hijacker)
	sw := httpResponseWriter{ResponseWriter: w, hijacker: hijacker}
	h.logRequest(h.H)(&sw, r)
}

type HealthCheckHandler struct {
	Healthy int32
}

func (h HealthCheckHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if atomic.LoadInt32(&h.Healthy) == 1 {
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Header().Set("X-Content-Type", "application/json; charset=utf-8")
		io.WriteString(w, `{"alive": true}`)

		return
	}
	w.WriteHeader(http.StatusServiceUnavailable)
}

type FwdToZapWriter struct {
	Logger *zap.Logger
}

func (fw *FwdToZapWriter) Write(p []byte) (n int, err error) {
	fw.Logger.Error(string(p))
	return len(p), nil
}
