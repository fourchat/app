module gitlab.com/fourchat/app

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/streadway/amqp v0.0.0-20190225234609-30f8ed68076e
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
)
