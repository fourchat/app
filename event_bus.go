package app

// System Bus

import (
	"github.com/streadway/amqp"
	"go.uber.org/zap"
	"os"
)

const EXCHANGE_NAME = "sysmsgs"

type EventBus struct {
	Consumers map[*EventConsumer]bool
	Pub       chan []byte

	Register   chan *EventConsumer
	Unregister chan *EventConsumer

	Log *zap.Logger
}

type EventMessage struct {
	Type    string      `json:"type"`
	UserId  int64       `json:"user_id"`
	Payload interface{} `json:"payload"`
}

func NewBus(log *zap.Logger) *EventBus {
	return &EventBus{
		Consumers:  make(map[*EventConsumer]bool),
		Pub:        make(chan []byte),
		Register:   make(chan *EventConsumer),
		Unregister: make(chan *EventConsumer),

		Log: log,
	}
}

func (b *EventBus) Run() {
	log := b.Log
	log.Debug("EventBus publish running...")

	conn, err := amqp.Dial(os.Getenv("FOURCHAT_AMQP_CONNECT_SPEC"))
	if err != nil {
		log.Error("cannot connect to AMQP: ", zap.Error(err))
		return
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Error("cannot open channel to AMQP: ", zap.Error(err))
		return
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		EXCHANGE_NAME, // name
		"fanout",      // type
		true,          // durable
		false,         // auto-deleted
		false,         // internal
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		log.Error("cannot open channel to AMQP: ", zap.Error(err))
		return
	}

	for {
		select {
		case message := <-b.Pub:
			log.Debug(" [x] ", zap.String("publish msg", string(message[:])))

			err = ch.Publish(
				EXCHANGE_NAME,
				"",
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        message,
				},
			)
			if err != nil {
				log.Error("Fail to publish: ", zap.String("message", string(message[:])))
			}
		case consumer := <-b.Register:
			log.Debug("Registered new consumer")
			b.Consumers[consumer] = true
		case consumer := <-b.Unregister:
			log.Debug("Deleted consumer")
			if _, ok := b.Consumers[consumer]; ok {
				delete(b.Consumers, consumer)
				close(consumer.Messages)
			}
		}
	}
}
